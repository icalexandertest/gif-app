export const GifItem = ({title, url}) => {

    return (
        <div className="card">
            <h2>{ title }</h2>
            <img alt={ title } src={ url }/>
        </div>
    )
}
